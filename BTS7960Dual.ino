#include "BTS7960.h"

const int btnMaju = 2;
const int btnMundur = 4;
const int btnKanan = 7;
const int btnKiri = 8;

const uint8_t EN_M1 = 3;
const uint8_t L_PWM_M1 = 5;
const uint8_t R_PWM_M1 = 6;

const uint8_t EN_M2 = 9;
const uint8_t L_PWM_M2 = 10;
const uint8_t R_PWM_M2 = 11;

int speed = 255;

BTS7960 Motor1(EN_M1, L_PWM_M1, R_PWM_M1);
BTS7960 Motor2(EN_M2, L_PWM_M2, R_PWM_M2);

void setup()
{
  Serial.begin(9600);
  
  pinMode(btnMaju, INPUT);
  pinMode(btnMundur, INPUT);
  pinMode(btnKanan, INPUT);
  pinMode(btnKiri, INPUT);

  digitalWrite(btnMaju, HIGH);
  digitalWrite(btnMundur, HIGH);
  digitalWrite(btnKanan, HIGH);
  digitalWrite(btnKiri, HIGH);
}

void loop()
{
  int val = analogRead(A0);
  byte pwm = map(val, 0, 1023, 0, 255);
  speed = pwm;
  
  if (digitalRead(btnMaju) == LOW) {    
    maju();
    Serial.println("MAJU " + speed);
  }else if (digitalRead(btnMundur) == LOW) {
    mundur();
    Serial.println("MUNDUR " + speed);
  }else if (digitalRead(btnKanan) == LOW) {
    kanan();
    Serial.println("KANAN " + speed);
  }else if (digitalRead(btnKiri) == LOW) {
    kiri();
    Serial.println("KIRI " + speed);
  }else{
    berhenti();
    Serial.println("BERHENTI " + speed);
  }

  //delay(5000);
}

void maju() {
  Motor1.Enable();
  Motor2.Enable();

  //for (int speed = 0 ; speed < 255; speed += 10)
  //{
    Motor1.TurnRight(speed);
    Motor2.TurnRight(speed);
    //delay(100);
  //}
}

void mundur() {
  Motor1.Enable();
  Motor2.Enable();

  //for (int speed = 0 ; speed < 255; speed += 10)
  //{
    Motor1.TurnLeft(speed);
    Motor2.TurnLeft(speed);
    //delay(100);
  //}
}

void kanan() {
  Motor1.Enable();
  //Motor2.Enable();

  //for (int speed = 0 ; speed < 255; speed += 10)
  //{
    Motor1.TurnRight(speed);
    //Motor2.TurnLeft(speed);
    //delay(100);
  //}
}

void kiri() {
  //Motor1.Enable();
  Motor2.Enable();

  //for (int speed = 0 ; speed < 255; speed += 10)
  //{
    //Motor1.TurnLeft(speed);
    Motor2.TurnRight(speed);
    //delay(100);
  //}
}

void berhenti() {  
  Motor1.Stop();
  Motor2.Stop();

  Motor1.Disable();
  Motor2.Disable();
}
